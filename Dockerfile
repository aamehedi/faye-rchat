FROM rails:4.2.6
MAINTAINER Abdullah All Mehedi <whitepaper321@gmail.com>

RUN mkdir -p /var/app
WORKDIR /var/app
COPY Gemfile /var/app/Gemfile
COPY Gemfile.lock /var/app/Gemfile.lock
RUN bundle install
