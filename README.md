# Faye rChat #

It just a demo chat app to understand the tools and technologies related to chat application in ruby. 

### System Requirements ###

* DB: Any commonly used database, like mysql, postgres, sqlite, etc.
* Ruby: 2.2.3 (I used it. But you may able to use a downgraded version at your own risk.)
* Rails: 4.2.6 (I used it. But you may able to use a downgraded version at your own risk.)

### Installation instruction ###

*  run `bundle install`
* Update database configuration in /config/database.yml
* run `rake db:create && rake db:migrate`
* Update private pub configuration in /config/database.yml (especially server address) according to your environment.
* If you are in a development environment run `rails s -b 0.0.0.0`. In production environment run rails server normally.
* If you are in a development environment run `rackup private_pub.ru -o 0.0.0.0 -s thin -E production`. In production environment run `rackup private_pub.ru -s thin -E production`.