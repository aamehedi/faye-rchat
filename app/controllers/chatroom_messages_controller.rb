class ChatroomMessagesController < ApplicationController
  def create
    @chatroom_message = ChatroomMessage.new chatroom_mesages_params
    @chatroom_message.chatroom_id = params[:chatroom_id]
    @chatroom_message.user = current_user
    @chatroom_message.save
  end

  private
  def chatroom_mesages_params
    params.require(:chatroom_message).permit :content
  end
end
