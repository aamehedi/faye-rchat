class ChatroomsController < ApplicationController
  def index
    @chatroom = Chatroom.first
    redirect_to new_chatroom_path unless @chatroom
  end

  def show
  end

  def new
  end

  def create
    @chatroom = Chatroom.new chatroom_params
    @chatroom.user = current_user
    if @chatroom.save
      flash[:success] = "Chat room created successfully."
      redirect_to authenticated_root_path
    else
      render :new
    end
  end
  
  private
  def chatroom_params
    params.require(:chatroom).permit :name
  end
end
