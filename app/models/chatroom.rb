class Chatroom < ActiveRecord::Base
  belongs_to :user
  has_many :chatroom_messages, dependent: :destroy

  def recent_chat
    self.chatroom_messages.last(20).reverse
  end
end
