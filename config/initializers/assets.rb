# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css.scss, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
asset_names = []
full_path = Rails.root.join("app/assets/").to_s
Dir[Rails.root.join("app/controllers/**/*_controller.rb")].map do |path|
  controller = (path.match(/app\/controllers\/([\w\/]+)_controller.rb/); $1)
  unless controller == "application"
    asset_names << controller + ".js" if File.exists? full_path + "javascripts/" + controller + ".js"
    asset_names << controller + ".css" if File.exists? full_path + "stylesheets/" + controller + ".css"
  end
end
Rails.application.config.assets.precompile += asset_names
