Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: "registrations",
    sessions: "my_sessions"
  }
  authenticated :user do
    root to: "chatrooms#index", as: :authenticated_root
  end
  resources :chatrooms, except: [:edit, :update, :destroy] do
    resources :chatroom_messages, only: [:create, :destroy]
  end
  root "pages#index"
end
