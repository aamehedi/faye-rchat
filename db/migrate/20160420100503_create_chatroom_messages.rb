class CreateChatroomMessages < ActiveRecord::Migration
  def change
    create_table :chatroom_messages do |t|
      t.references :chatroom, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.text :content

      t.timestamps null: false
    end
  end
end
